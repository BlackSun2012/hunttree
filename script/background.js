/*****************************************************************************************************************************************
	Data Structure List

node
( object )

	tab
	( Tab )
		The node's tab.
	root
	( Object )
		The root of this node tree.
	father
	( node )
		This node's parent node in logic.
	children
	( Array )
		This node's child nodes in logic.
	childNum
	( int )
		Number of its child nodes.
	status
	( int )
		Whether the tab is still alive now.

*****************************************************************************************************************************************/
/** 定义node类 **/
function node(tab, root, father, children, childNum, status) 
{
	this.tab = tab;
	this.root = root;
	this.father = father;
	this.children = children;
	this.childNum = childNum;
	this.status = status;
}

/** 初始化 **/
/******************************************
 * 这里的root看上去好像没用               *
 * 然而事实上这是我给HuntTree留的一个伏笔 *
 * 以后要是打算把HuntTree做大的话就靠它了 *
 *****************************************/
var root = new node({id:0}, this, this, "", 0, 1);
var currentNode;


/* 这个函数使用来获得浏览器的第一组tab (Under construction) */
chrome.windows.getCurrent({populate:true}, function (window) {
	root.children = new Array();
	root.childNum = 0;
	for (var i = 0; i < window.tabs.length; i++) {
		root.children[i] = new node(window.tabs[i], root, root, "", 0, 1);
		root.childNum++;
		if (root.children[i].tab.active == true)
			currentNode = root.children[i];
	}
});

/** 函数 **/
function addNode(tab) {
	var newNode = new node(tab, root, currentNode, "", 0, 1);
	if (typeof(currentNode.children) != "object")
		currentNode.children = new Array(newNode);
	else
		currentNode.children[currentNode.childNum] = newNode;
	currentNode.childNum++;
	/*
	alert("Done:\nnodeid:\t" + newNode.tab.id
			+ "\nurl:\t" + newNode.tab.url
			+ "\nfatherid:\t" + newNode.father.tab.id
			+ "\nfatherurl\t: " + newNode.father.tab.url
			+ "\nchildNum:\t" + newNode.childNum
			+ "\nfatherchildNum:\t" + newNode.father.childNum
			);
	*/
}

/* setCurrent:  设置当前节点 */
function setCurrent(tab) {
	currentNode = search(tab.id, root);
	//alert("set current node:" + currentNode.tab.id + "\nIt's father is:" + currentNode.father.tab.id);
}

/* search: 根据tabId拿到节点 */
function search(tabId, tree) {
	var tmp;
	//alert("now we are in node:" + tree.tab.id);
	if (tree.tab.id == tabId)
		return tree;
	else
		for (var i = 0; i < tree.childNum; i++)
			if ((tmp = search(tabId, tree.children[i])) != false)
				return tmp;
	return false;
}

/* active: 显示选中的Tab (Bate)*/
function active (to) {
	chrome.tabs.update(to.tab.id, {active:true});
}

/* goLeft: 切换到逻辑左节点 */
function goLeft (tmp) {
	if (tmp.father != root) {
		for (var i = 0; i < tmp.father.childNum; i++)
			if (tmp.father.children[i].tab.id == tmp.tab.id)
				break;
		if (i == 0)
			alert("This is the first child.");
		else {
			for (i--; i >= 0; i--)
				if (tmp.father.children[i].status == 1)
					break;
			if (i >= 0)
				active(tmp.father.children[i]);
			else
				alert("逻辑左侧的节点全部已经关闭。");
		}
	}
	else
		alert("This is the first node in the tree.");
}

/* goRight: 切换到逻辑右节点 */
function goRight (tmp) {
	if (tmp.father != root) {
		for (var i = 0; i < tmp.father.childNum; i++)
			if (tmp.father.children[i].tab.id == tmp.tab.id)
				break;
		if (i + 1 == tmp.father.childNum)
			alert("This is the last child.");
		else {
			for (i++; i < tmp.father.childNum; i++) 
				if (tmp.father.children[i].status == 1)
					break;
			if (i < tmp.father.childNum)
				active(tmp.father.children[i]);
			else
				alert("逻辑右侧的节点全部已经关闭。");
		}
	}
	else
		alert("This is the first node in the tree.");
}

/* goUp: 切换到逻辑根节点 */
function goUp (tmp) {
	if (tmp.father == root)
		alert("This is the first page.");
	else
		if (tmp.father.status == 1)
			active(tmp.father);
		else
			goUp(tmp.father);
}

/* goDown: 切换到逻辑子节点 (正在施工)*/
function goDown(tmp) {
/*
	if (tmp.childNum > 0)
		active(tmp.children[0]);
	else
		alert("It has no childern now.");
*/
	if (tmp.childNum > 0) {
		var toActive = findDown(tmp);
		active(toActive);
	}
	else
		alert("It has no childern now.");
}

/* findDown: 寻找仍在线的最近子节点 */
function findDown(tmp) {
	var goal;
	for (var i = 0; i < tmp.childNum; i++)
		if (tmp.children[i].status == 1)
			return tmp.children[i];
		else if ((goal = findDown(tmp.children[i])) != false)
			return goal;
	return false;
}

/* showInfo: 显示tab信息, 用于调试
function showInfo(tab) {
	alert("tabInfo: " + "\n"
		+ "tab.id: " + tab.id + "\n"
		+ "tab.index: " + tab.index + "\n"
		+ "tab.windowId: " + tab.windowId + "\n"
		+ "tab.openerTabId: " + tab.openerTabId + "\n"
		+ "tab.highlighted: " + tab.highlighted + "\n"
		+ "tab.active: " + tab.active + "\n"
		+ "tab.pinned: " + tab.pinned + "\n"
		+ "tab.url: " + tab.url + "\n"
		+ "tab.title: " + tab.title + "\n"
		+ "tab.favIconUrl: " + tab.favIconUrl + "\n"
		+ "tab.status: " + tab.status + "\n"
		+ "tab.incognito: " + tab.incognito + "\n"
	);
}
*/
/******************************************************************************************************************************************
 ********************************************************************* 监听器接口 *********************************************************
 ******************************************************************************************************************************************/
function onCreated(tab) {
	/*
	alert("current tab: " + currentNode.tab.id + "\n" +
			"will create Node: " + tab.url);
	*/
	addNode(tab);
}

function onActivated(activeInfo) {
	chrome.tabs.get(activeInfo.tabId, setCurrent);
}

function onRemoved(tabId, changeInfo, tab) {
	search(tabId, root).status = 0;
}

// 正在施工(Bate)
function onRequest(keyInfo) {
	/* 调试使用
	alert("You pressed: " + keyInfo.which 
			+ "\nIs shift: " + keyInfo.shift 
			+ "\nIs ctrl: " + keyInfo.ctrl
			+ "\nIs alt: " + keyInfo.alt
	);
	*/
	if (keyInfo.which == 72 && keyInfo.alt == true) {
		//alert("H and alt");	
		goLeft(currentNode);
	}
	else if (keyInfo.which == 74 && keyInfo.alt == true) {
		//alert("J and alt");
		goDown(currentNode);
	}
	else if (keyInfo.which == 75 && keyInfo.alt == true) {
		//alert("K and alt")t;
		goUp(currentNode);
		}
	else if (keyInfo.which == 76 && keyInfo.alt == true) {
		//alert("L and alt");
		goRight(currentNode);
	}
	/* Ctrl + I: 显示tab信息， 用于调试
	else if (keyInfo.which == 73 && keyInfo.alt == true) {
		//alert("I and ctrl");
		showInfo(currentNode.tab);
	}
	*/
	else;
}

/******************************************************************************************************************************************
 ********************************************************************* 监听器 *********************************************************
 ******************************************************************************************************************************************/
chrome.tabs.onActivated.addListener(onActivated);
chrome.tabs.onCreated.addListener(onCreated);
chrome.tabs.onRemoved.addListener(onRemoved);
chrome.extension.onRequest.addListener(onRequest);
