window.onkeydown = keyPress;

function keyPress (evt) {
	if (evt.altKey == true) {
		chrome.extension.sendRequest({
			"which": evt.which, 
			"shift": evt.shiftKey, 
			"ctrl": evt.ctrlKey,
			"alt": evt.altKey
		});
	}
}
